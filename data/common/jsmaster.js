// Copyright 2001 ClickAgents, Inc. All rights reserved.

CALoaded = true;
CAFullVersion = CAVersion + ".10";

function CAShowAd() {
  
  CAOptions = '&v=' + CAFullVersion;
  CAFullOptions = '';
  CATextTest = 0;
  if (self.CACategory) CAOptions += '&c=' + self.CACategory;
  if (self.CABorder)   CAOptions += '&border=1';
  if (self.CAPlacement) CAFullOptions += '&p=' + self.CAPlacement; 
  if (self.CAKWParam)  CAOptions += '&r=' + self.CAKWParam;
  if (self.CAKeywords) CAOptions += '&k=' + self.CAKeywords;

  if ((self.CAWidth == null) || (self.CAHeight == null)) {
      CAWidth  = 468;
      CAHeight = 60;
  }

  //Do not show text for non-standard banners. CANoTest must come after this test.
  if (self.CAWidth == 468 && self.CAHeight == 60) {
    CATextTest = 0;
  }else {
    if(! self.CANoText) CATextTest = 1; 
    self.CANoText = true;
  }

  if (! self.CANoText) CAOptions += '&text=1';
  if (self.CATargetCurrent) CAOptions += '&target=self';

  CARandom   = Math.round(Math.random()*1000) + 1;
  CAHostInfo = "host=" + CAHost + "&b=" + CAID + "." + CARandom;

  if (self.CAServer == null) CAServer = "ads";

  CAFullServer   = "http://" + CAServer + ".clickagents.com/";

  CASize = '&size=' + CAWidth + 'x' + CAHeight;

  CABanner   = CAFullServer + 'advance.ca?' + CAHostInfo + CAOptions + CAFullOptions + CASize;
  CARedirect = CAFullServer + 'redirect?' + CAHostInfo + CAFullOptions + CASize;

  CADimensions();

  if (CATextTest == 1) self.CANoText = false;

  if (navigator.userAgent.indexOf("MSIE") >= 0) {
    // don't try to set the bgcolor etc in the IFRAME for MSIE 3 
    if (navigator.appVersion.indexOf('MSIE 3') < 0) {
      if (self.CABgColor)    CABanner += '&bgcolor='    + escape(self.CABgColor);
      if (self.CALinkColor)  CABanner += '&linkcolor='  + escape(self.CALinkColor);
      if (self.CAAlinkColor) CABanner += '&alinkcolor=' + escape(self.CAAlinkColor);
      if (self.CAVlinkColor) CABanner += '&vlinkcolor=' + escape(self.CAVlinkColor);
    }
     document.write('<SCRIPT SRC="' + CABanner + '&t=js"');
     document.write(' LANGUAGE="JavaScript"></SCR' + 'IPT>');
  } else {
    // should be all Netscapes that are reading this file 
	if (self.CAVersion == 1.0 && parseInt(navigator.appVersion) < 5 ) {
          document.write('<table border=0 cellpadding=0 cellspacing=0><tr><td>');
	  document.write('<ILAYER ID="VC" VISIBILITY="hide" BGCOLOR="" WIDTH="' + IWidth);
          document.write('" HEIGHT="' + IHeight + '"></ILAYER>');
	  document.write('</td></tr></table>');
	} else {
	  document.write('<SCRIPT SRC="' + CABanner + '&t=js"');
	  document.write(' LANGUAGE="JavaScript"></SCR' + 'IPT>');
    }
  }
}

function CADimensions() {
  if (self.CANoText) {
    if (self.CABorder) {
      IWidth  = CAWidth + 4;  
      IHeight = CAHeight + 4;
    } else {
      IWidth  = CAWidth;  
      IHeight = CAHeight;
    }       
  } else {
    if (self.CABorder) {
      IWidth  = CAWidth + 4;
      IHeight = CAHeight + 24;
    } else {
      IWidth  = CAWidth;
      IHeight = CAHeight + 24;
    }       
  }
}
