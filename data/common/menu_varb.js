/***********************************************************************************
*	(c) Gennadios 2003                                                    *
***********************************************************************************/

	var NoOffFirstLineMenus=3;			// Number of first level items
	var LowBgColor='DCCBCB';			// Background color when mouse is not over
	var LowSubBgColor='DCCBCB';		// Background color when mouse is not over on subs
	var HighBgColor='FFFFFF';			// Background color when mouse is over
	var HighSubBgColor='FFFFFF';			// Background color when mouse is over on subs
	var FontLowColor='993300';			// Font color when mouse is not over
	var FontSubLowColor='993300';		// Font color subs when mouse is not over
	var FontHighColor='993300';			// Font color when mouse is over
	var FontSubHighColor='993300';		// Font color subs when mouse is over
	var BorderColor='993300';			// Border color
	var BorderSubColor='993300';			// Border color for subs
	var BorderWidth=2;				// Border width
	var BorderBtwnElmnts=1;			// Border between elements 1 or 0
	var FontFamily="tahoma,comic sans ms,technical"	// Font family menu items
	var FontSize=10;				// Font size menu items
	var FontBold=1;				// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered='left';			// Item text position 'left', 'center' or 'right'
	var MenuCentered='left';			// Menu horizontal position 'left', 'center' or 'right'
	var MenuVerticalCentered='top';		// Menu vertical position 'top', 'middle','bottom' or static
	var ChildOverlap=.3;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=.2;			// vertical overlap child/ parent
	var StartTop=01;				// Menu offset x coordinate
	var StartLeft=01;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=0;				// Multiple frames x correction
	var LeftPaddng=3;				// Left padding
	var TopPaddng=5;				// Top padding
	var FirstLineHorizontal=0;			// SET TO 1 FOR HORIZONTAL MENU, 0 FOR VERTICAL
	var MenuFramesVertical=1;			// Frames in cols or rows 1 or 0
	var DissapearDelay=800;			// delay before menu folds in
	var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
	var FirstLineFrame='navig';			// Frame where first level appears
	var SecLineFrame='space';			// Frame where sub levels appear
	var DocTargetFrame='space';			// Frame where target documents appear
	var TargetLoc='';				// span id for relative positioning
	var HideTop=0;				// Hide first level when loading new document 1 or 0
	var MenuWrap=1;				// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var WebMasterCheck=0;			// menu tree checking on or off 1 or 0
	var ShowArrow=1;				// Uses arrow gifs when 1
	var KeepHilite=1;				// Keep selected path highligthed
	var Arrws=['common/tri.gif',5,10,'common/tridown.gif',10,5,'common/trileft.gif',5,10];	// Arrow source, width and height

function BeforeStart(){return}
function AfterBuild(){return}
function BeforeFirstOpen(){return}
function AfterCloseAll(){return}


// Menu tree
//	MenuX=new Array(Text to show, Link, background image (optional), number of sub elements, height, width);
//	For rollover images set "Text to show" to:  "rollover:Image1.jpg:Image2.jpg"

Menu1=new Array("������ �������","","",7,20,200);
	Menu1_1=new Array("� ����������","","",2,25,200);
		Menu1_1_1=new Array("������� �������","data/bibliothiki/a/dimiourgia/1.htm","",0,25,200);
		Menu1_1_2=new Array("�������� �������","data/bibliothiki/keimena/palaia.htm","",0,25,200);
	Menu1_2=new Array("�� ����������","","",2,25,200);
		Menu1_2_1=new Array("������� �������","data/bibliothiki/a/patriarxes/1.htm","",0,25,200);
		Menu1_2_2=new Array("�������� �������","data/bibliothiki/keimena/palaia17.htm","",0,25,200);
	Menu1_3=new Array("� ������","","",2,25,200);
		Menu1_3_1=new Array("������� �������","data/bibliothiki/a/exodos/1.htm","",0,25,200);
		Menu1_3_2=new Array("�������� �������","data/bibliothiki/keimena/palaia20.htm","",0,25,200);
	Menu1_4=new Array("�� ��������",".htm","",2,25,200);
		Menu1_4_1=new Array("������� �������","data/bibliothiki/a/basileis/1.htm","",0,25,200);
		Menu1_4_2=new Array("�������� �������","data/bibliothiki/keimena/palaia29.htm","",0,25,200);
	Menu1_5=new Array("�� ��������","","",2,25,200);
		Menu1_5_1=new Array("������� �������","data/bibliothiki/a/profites/1.htm","",0,25,200);
		Menu1_5_2=new Array("�������� �������","data/bibliothiki/keimena/palaia34.htm","",0,25,200);
    Menu1_6=new Array("���������� - ����������","data/bibliothiki/a/gramatis/1.htm","",0,25,480);
	Menu1_7=new Array("������","","",11,25,480);       
	 Menu1_7_1=new Array("�� ������ ��� ������","data/bibliothiki/a/xartes/1.htm","",0,25,300);
	 Menu1_7_2=new Array("� ������","data/bibliothiki/a/xartes/2.htm","",0,25,300);
     Menu1_7_3=new Array("�� ������ ����� ��� ������","data/bibliothiki/a/xartes/3.htm","",0,25,300);
     Menu1_7_4=new Array("�� �������� ��� ����� ��� ��� ���������","data/bibliothiki/a/xartes/4.htm","",0,25,300);
     Menu1_7_5=new Array("�� �������� ��� ������ ��� ��� �����","data/bibliothiki/a/xartes/5.htm","",0,25,300);
     Menu1_7_6=new Array("�� �������� ��� �����","data/bibliothiki/a/xartes/6.htm","",0,25,300);
     Menu1_7_7=new Array("� ���������� ������������","data/bibliothiki/a/xartes/7.htm","",0,25,300);
     Menu1_7_8=new Array("� ��������� ������������","data/bibliothiki/a/xartes/8.htm","",0,25,300);
	 Menu1_7_10=new Array("� ������� ������������","data/bibliothiki/a/xartes/9.htm","",0,25,300);
     Menu1_7_9=new Array("� ����������� ������������","data/bibliothiki/a/xartes/10.htm","",0,25,300);
     Menu1_7_11=new Array("� ������������ ��� ������� ����������","data/bibliothiki/a/xartes/11.htm","",0,25,300);
	 
 Menu2=new Array("����� �������","","",5,20,200);              
	Menu2_1=new Array("�� �������","","",2,25,300);
		Menu2_1_1=new Array("������� �������","data/bibliothiki/b/aparxes/1.htm","",0,25,200);
		Menu2_1_2=new Array("�������� �������","data/bibliothiki/keimena/kaini.htm","",0,25,200);
	Menu2_2=new Array("� ���������� ��� �������","","",2,25,300);
		Menu2_2_1=new Array("������� �������","data/bibliothiki/b/didaskalia/1.htm","",0,25,200);
		Menu2_2_2=new Array("�������� �������","data/bibliothiki/keimena/kaini53.htm","",0,25,200);
	Menu2_3=new Array("�� ������� ��� �������","","",2,25,300);
		Menu2_3_1=new Array("������� �������","data/bibliothiki/b/thaumata/1.htm","",0,25,200);
		Menu2_3_2=new Array("�������� �������","data/bibliothiki/keimena/kaini43.htm","",0,25,200);
	Menu2_4=new Array("�� ����� ��� � �������� ��� �������","","",2,25,300);
		Menu2_4_1=new Array("������� �������","data/bibliothiki/b/anastasi/1.htm","",0,25,200);
		Menu2_4_2=new Array("�������� �������","data/bibliothiki/keimena/kaini82.htm","",0,25,200);
	Menu2_5=new Array("������","","",4,25,300);
               Menu2_5_1=new Array("� ������� ������������ ��� ����� ��� ������ ��������","data/bibliothiki/b/xartes/1.htm","",0,25,380);
               Menu2_5_2=new Array("�� �������� ��� �����","data/bibliothiki/b/xartes/2.htm","",0,25,380);
               Menu2_5_3=new Array("� ������� ��� �����","data/bibliothiki/b/xartes/3.htm","",0,25,380);
			   Menu2_5_4=new Array("� ���������� ���� ����� ��� ������ ��������","data/bibliothiki/b/xartes/4.htm","",0,25,380);
               

Menu3=new Array("� ������� ��� ���������","","",7,20,200);
	Menu3_1=new Array("�������","","",2,25,350);
		Menu3_1_1=new Array("������� �������","data/bibliothiki/c/aparxes/1.htm","",0,25,200);
		Menu3_1_2=new Array("�������� �������","data/bibliothiki/keimena/history1.htm","",0,25,200);
	Menu3_2=new Array("��������, ������� ��� ����������","data/bibliothiki/c/diogmoi/1.htm","",0,25,480);
	Menu3_3=new Array("�������� ��� ����","data/bibliothiki/c/edraiwsi/1.htm","",0,25,480);
	Menu3_4=new Array("��������� �������� ���� ������","data/bibliothiki/c/europe/1.htm","",0,25,480);
	Menu3_5=new Array("������� ������","data/bibliothiki/c/neotera_xronia/1.htm","",0,25,480);
	Menu3_6=new Array("� ������������ ������ ������","data/bibliothiki/c/xris_simera/1.htm","",0,25,480);
	Menu3_7=new Array("������","","",14,25,480);
		Menu3_7_1=new Array("� ���������� ��� ������","data/bibliothiki/c/xartes/1.htm","",0,25,380);
		Menu3_7_2=new Array("�� ���������� ������� ��� ������","data/bibliothiki/c/xartes/2.htm","",0,25,280);
		Menu3_7_3=new Array("� �������� ��� �������� �������������","data/bibliothiki/c/xartes/3.htm","",0,25,280);
		Menu3_7_4=new Array("� ������ ���� ����� ��� ������ ��������","data/bibliothiki/c/xartes/4.htm","",0,25,280);
		Menu3_7_5=new Array("� ���������� �������� ��� �����������������","data/bibliothiki/c/xartes/5.htm","",0,25,280);
		Menu3_7_6=new Array("�� ������ ��������� ������������ �������","data/bibliothiki/c/xartes/6.htm","",0,25,380);
		Menu3_7_7=new Array("� ������� ������������","data/bibliothiki/c/xartes/7.htm","",0,25,280);
		Menu3_7_8=new Array("� ��������� ������������ �� 565 �.�.","data/bibliothiki/c/xartes/9.htm","",0,25,280);
		Menu3_7_9=new Array("� ��������� ������������ �� 780 �.�.","data/bibliothiki/c/xartes/10.htm","",0,25,280);
		Menu3_7_10=new Array("� ��������� ������������ �� 1092 �.�.","data/bibliothiki/c/xartes/11.htm","",0,25,280);
		Menu3_7_11=new Array("�� �������� ���� �� ������������ (1218 �.�.)","data/bibliothiki/c/xartes/12.htm","",0,25,280);
		Menu3_7_12=new Array("� ��������� ������������ �� 1453 �.�.","data/bibliothiki/c/xartes/13.htm","",0,25,280);
		Menu3_7_13=new Array("� ��������� ������������","data/bibliothiki/c/xartes/8.htm","",0,25,280);
		Menu3_7_14=new Array("������� ��������� �� ���������","data/bibliothiki/c/xartes/14.htm","",0,25,280);



