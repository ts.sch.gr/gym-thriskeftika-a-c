/***********************************************************************************
*	(c) Gennadios 2003                                                    *
***********************************************************************************/

	var NoOffFirstLineMenus=1;			// Number of first level items
	var LowBgColor='DCCBCB';			// Background color when mouse is not over
	var LowSubBgColor='DCCBCB';		// Background color when mouse is not over on subs
	var HighBgColor='FFFFFF';			// Background color when mouse is over
	var HighSubBgColor='FFFFFF';			// Background color when mouse is over on subs
	var FontLowColor='993300';			// Font color when mouse is not over
	var FontSubLowColor='993300';		// Font color subs when mouse is not over
	var FontHighColor='993300';			// Font color when mouse is over
	var FontSubHighColor='993300';		// Font color subs when mouse is over
	var BorderColor='993300';			// Border color
	var BorderSubColor='993300';			// Border color for subs
	var BorderWidth=2;				// Border width
	var BorderBtwnElmnts=1;			// Border between elements 1 or 0
	var FontFamily="tahoma,comic sans ms,technical"	// Font family menu items
	var FontSize=10;				// Font size menu items
	var FontBold=1;				// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered='left';			// Item text position 'left', 'center' or 'right'
	var MenuCentered='left';			// Menu horizontal position 'left', 'center' or 'right'
	var MenuVerticalCentered='top';		// Menu vertical position 'top', 'middle','bottom' or static
	var ChildOverlap=.1;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=.1;			// vertical overlap child/ parent
	var StartTop=05;				// Menu offset x coordinate
	var StartLeft=180;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=0;				// Multiple frames x correction
	var LeftPaddng=3;				// Left padding
	var TopPaddng=2;				// Top padding
	var FirstLineHorizontal=0;			// SET TO 1 FOR HORIZONTAL MENU, 0 FOR VERTICAL
	var MenuFramesVertical=1;			// Frames in cols or rows 1 or 0
	var DissapearDelay=1000;			// delay before menu folds in
	var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
	var FirstLineFrame='navig';			// Frame where first level appears
	var SecLineFrame='space';			// Frame where sub levels appear
	var DocTargetFrame='space';			// Frame where target documents appear
	var TargetLoc='';				// span id for relative positioning
	var HideTop=0;				// Hide first level when loading new document 1 or 0
	var MenuWrap=1;				// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var WebMasterCheck=0;			// menu tree checking on or off 1 or 0
	var ShowArrow=1;				// Uses arrow gifs when 1
	var KeepHilite=1;				// Keep selected path highligthed
	var Arrws=['common/tri.gif',5,10,'common/tridown.gif',10,5,'common/trileft.gif',5,10];	// Arrow source, width and height

function BeforeStart(){return}
function AfterBuild(){return}
function BeforeFirstOpen(){return}
function AfterCloseAll(){return}


// Menu tree
//	MenuX=new Array(Text to show, Link, background image (optional), number of sub elements, height, width);
//	For rollover images set "Text to show" to:  "rollover:Image1.jpg:Image2.jpg"

Menu1=new Array("����� �������","","",5,20,200);
	Menu1_1=new Array("�� �������� ������������","chapters/1/a/1.htm","",4,25,300);
   	        Menu1_1_1=new Array("� ������������ ��������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_1_2=new Array("� ������������ ������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_1_3=new Array("� ������������ �������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_1_4=new Array("� ������������ ��������","chapters/1/a/1.htm","",0,25,200);
	Menu1_2=new Array("�� �������","chapters/1/a/1.htm","",8,25,300);
   	        Menu1_2_1=new Array("� ������������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_2_2=new Array("� ����� ��� � �������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_2_3=new Array("� �������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_2_4=new Array("� ��������","chapters/1/a/1.htm","",0,25,200);
                     Menu1_2_5=new Array("� ���� ���� �������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_2_6=new Array("� ������� � ���������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_2_7=new Array("� ������� ��� �������","chapters/1/a/1.htm","",0,25,200);
   	        Menu1_2_8=new Array("�� ������ ���������","chapters/1/a/1.htm","",0,25,200);
             Menu1_3=new Array("� ���������� ��� �������","chapters/1/b/1.htm","",5,25,200);
                     Menu1_3_1=new Array("� ��� ��� ����� ������","chapters/1/b/1.htm","",0,25,200);
                     Menu1_3_2=new Array("� �������� ��� ������ ����","chapters/1/b/1.htm","",0,25,200);
                     Menu1_3_3=new Array("� �������� ��� ����� ���������","chapters/1/b/1.htm","",0,25,200);
                     Menu1_3_4=new Array("� �������� ��� ���� ��������","chapters/1/b/1.htm","",0,25,200);
                     Menu1_3_5=new Array("� �������� �� �� �����������","chapters/1/b/1.htm","",0,25,200);
	Menu1_4=new Array("�� ������� ��� �������","chapters/1/b_1/1.htm","",0,25,400);
	Menu1_5=new Array("�� ����� ��� � �������� ��� �������","chapters/1/c/1.htm","",0,25,400);
	
	


