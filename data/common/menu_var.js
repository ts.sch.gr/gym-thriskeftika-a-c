/***********************************************************************************
*	(c) Gennadios 2003                                                    *
***********************************************************************************/

	var NoOffFirstLineMenus=1;			// Number of first level items
	var LowBgColor='DCCBCB';			// Background color when mouse is not over
	var LowSubBgColor='DCCBCB';		// Background color when mouse is not over on subs
	var HighBgColor='FFFFFF';			// Background color when mouse is over
	var HighSubBgColor='FFFFFF';			// Background color when mouse is over on subs
	var FontLowColor='993300';			// Font color when mouse is not over
	var FontSubLowColor='993300';		// Font color subs when mouse is not over
	var FontHighColor='993300';			// Font color when mouse is over
	var FontSubHighColor='993300';		// Font color subs when mouse is over
	var BorderColor='993300';			// Border color
	var BorderSubColor='993300';			// Border color for subs
	var BorderWidth=2;				// Border width
	var BorderBtwnElmnts=1;			// Border between elements 1 or 0
	var FontFamily="tahoma,comic sans ms,technical"	// Font family menu items
	var FontSize=8;				// Font size menu items
	var FontBold=1;				// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered='left';			// Item text position 'left', 'center' or 'right'
	var MenuCentered='left';			// Menu horizontal position 'left', 'center' or 'right'
	var MenuVerticalCentered='top';		// Menu vertical position 'top', 'middle','bottom' or static
	var ChildOverlap=.2;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=.2;			// vertical overlap child/ parent
	var StartTop=05;				// Menu offset x coordinate
	var StartLeft=50;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=0;				// Multiple frames x correction
	var LeftPaddng=3;				// Left padding
	var TopPaddng=5;				// Top padding
	var FirstLineHorizontal=0;			// SET TO 1 FOR HORIZONTAL MENU, 0 FOR VERTICAL
	var MenuFramesVertical=0;			// Frames in cols or rows 1 or 0
	var DissapearDelay=1000;			// delay before menu folds in
	var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
	var FirstLineFrame='navig';			// Frame where first level appears
	var SecLineFrame='space';			// Frame where sub levels appear
	var DocTargetFrame='space';			// Frame where target documents appear
	var TargetLoc='';				// span id for relative positioning
	var HideTop=0;				// Hide first level when loading new document 1 or 0
	var MenuWrap=1;				// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var WebMasterCheck=0;			// menu tree checking on or off 1 or 0
	var ShowArrow=1;				// Uses arrow gifs when 1
	var KeepHilite=1;				// Keep selected path highligthed
	var Arrws=['../../common/tri.gif',5,10,'../../common/tridown.gif',10,5,'../../common/trileft.gif',5,10];	// Arrow source, width and height

function BeforeStart(){return}
function AfterBuild(){return}
function BeforeFirstOpen(){return}
function AfterCloseAll(){return}


// Menu tree
//	MenuX=new Array(Text to show, Link, background image (optional), number of sub elements, height, width);
//	For rollover images set "Text to show" to:  "rollover:Image1.jpg:Image2.jpg"

Menu1=new Array("�����������","","",3,20,150);
 Menu1_1=new Array("������ �������","","",5,20,150);
 	Menu1_1_1=new Array("� ����������","","",2,25,150);
                     Menu1_1_1_1=new Array("� ������� ��� ������","picture/49.htm","",0,25,180);
					 Menu1_1_1_2=new Array("� ������������ ����","picture/adam.htm","",0,25,180);
	Menu1_1_2=new Array("�� ����������","","",4,25,200);
                     Menu1_1_2_1=new Array("� ���������� ������","picture/abraam.htm","",0,25,180);
                     Menu1_1_2_2=new Array("� ����� ��� ������","picture/thisiaabraam.htm","",0,25,180);
                     Menu1_1_2_3=new Array("� ���������� �����","picture/isaak.htm","",0,25,180);
                     Menu1_1_2_4=new Array("� ���������� �����","picture/iakov.htm","",0,25,180);
              Menu1_1_3=new Array("� ������","","",4,25,300);
	        Menu1_1_3_1=new Array("� ������","picture/54.htm","",0,25,180);
			Menu1_1_3_2=new Array("� ��������� �����","picture/vatos.htm","",0,25,180);
			Menu1_1_3_3=new Array("� ���������","picture/nomodosia.htm","",0,25,180);
			Menu1_1_3_4=new Array("� �����","picture/aaron.htm","",0,25,180);
			
	Menu1_1_4=new Array("�� ��������","","",1,25,300);
                     Menu1_1_4_1=new Array("�� �������� ����� ��� �������","picture/55.htm","",0,25,180);
	Menu1_1_5=new Array("�� ��������","","",6,25,300);
	        Menu1_1_5_1=new Array("� �������� ������","picture/56a.htm","",0,25,180);
	        Menu1_1_5_2=new Array("� �������� ��������","picture/53.htm","",0,25,180);
	        Menu1_1_5_3=new Array("� �������� �������","picture/57.htm","",0,25,180);
	        Menu1_1_5_4=new Array("� �������� ������","picture/58.htm","",0,25,180);
	        Menu1_1_5_5=new Array("� �������� ��������","picture/58a.htm","",0,25,180);
			Menu1_1_5_6=new Array("� �������� ��������","picture/52.htm","",0,25,180);
 Menu1_2=new Array("����� �������","","",5,20,150);
Menu1_2_1=new Array("��������� ��� ������������","","",5,25,230);
   	        Menu1_2_1_1=new Array("�� ���� ���������","picture/75.htm","",0,25,200);
   	        Menu1_2_1_2=new Array("� ������������ ��������","picture/mathaios.htm","",0,25,200);
   	        Menu1_2_1_3=new Array("� ������������ ������","picture/markos.htm","",0,25,200);
   	        Menu1_2_1_4=new Array("� ������������ ������","picture/loukas.htm","",0,25,200);
			Menu1_2_1_5=new Array("� ������������ �������","picture/ioannis.htm","",0,25,200);
Menu1_2_2=new Array("�� �������","","",13,25,300);
   	        Menu1_2_2_1=new Array("� ������� ��� ��������","picture/48.htm","",0,25,200);
   	        Menu1_2_2_2=new Array("�� ������� ��� ��������","picture/43.htm","",0,25,200);
   	        Menu1_2_2_3=new Array("� ������������ ��� ��������","picture/59.htm","",0,25,200);
   	        Menu1_2_2_4=new Array("� ���������� �������","picture/32.htm","",0,25,200);
            Menu1_2_2_5=new Array("� �������� ��������","picture/25.htm","",0,25,200);
   	        Menu1_2_2_6=new Array("� ��������� ��� �������","picture/62.htm","",0,25,200);
   	        Menu1_2_2_7=new Array("� ������� ��� ��������","picture/28.htm","",0,25,200);
			Menu1_2_2_8=new Array("� ������� ��� �������","picture/genisi.htm","",0,25,200);
			Menu1_2_2_9=new Array("� ��������","picture/ipapanti.htm","",0,25,200);
			Menu1_2_2_10=new Array("� ���� ���� �������","picture/73.htm","",0,25,200);
			Menu1_2_2_11=new Array("� ����� ��� � �������","picture/iosif_maria.htm","",0,25,200);
			Menu1_2_2_12=new Array("� ������� ��� �������","picture/vaptisi.htm","",0,25,200);
			Menu1_2_2_13=new Array("� ������� � ���������","picture/21.htm","",0,25,200);
Menu1_2_3=new Array("� ���������� ��� �������","","",6,25,200);
                     Menu1_2_3_1=new Array("� ������ �������","picture/20.htm","",0,25,230);
                     Menu1_2_3_2=new Array("� �����������","picture/pantokrator.htm","",0,25,230);
                     Menu1_2_3_3=new Array("� ��� ��� ����� ������","picture/omilia.htm","",0,25,230);
                     Menu1_2_3_4=new Array("� �������� ��� ������ ����","picture/asotos.htm","",0,25,230);
                     Menu1_2_3_5=new Array("� �������� ��� ����� ���������","picture/samaritis.htm","",0,25,230);
					 Menu1_2_3_6=new Array("� �������� ��� ���� ��������","picture/parthenes.htm","",0,25,230);
					
Menu1_2_4=new Array("�� ������� ��� �������","","",3,25,400);
	       Menu1_2_4_1=new Array("� ����������� ��� �������","picture/metamorfosi.htm","",0,25,200);
	       Menu1_2_4_2=new Array("� �������� ��� �������","picture/lazarus.htm","",0,25,200);
		   Menu1_2_4_3=new Array("� ������� �� ��� ������� ���","picture/agiamariaagiosnikodimos.htm","",0,25,200);
Menu1_2_5=new Array("�� ���� ���� ��� � ��������","","",17,25,400);
	       Menu1_2_5_1=new Array("� ������� ��� ����������","picture/baioforos.htm","",0,25,300);
	       Menu1_2_5_2=new Array("� �������� �������","picture/mistikos.htm","",0,25,300);
	       Menu1_2_5_3=new Array("� �������� ��� ������ �����","picture/60.htm","",0,25,300);
	       Menu1_2_5_4=new Array("� ����� ��� ����","picture/76.htm","",0,25,300);
	       Menu1_2_5_5=new Array("� ����� ������","picture/niptir.htm","",0,25,300);
	       Menu1_2_5_6=new Array("� ��������� ��������","picture/proseuxi.htm","",0,25,300);	
	       Menu1_2_5_7=new Array("� �������� ��� � �������","picture/prodosia.htm","",0,25,300);
	       Menu1_2_5_8=new Array("� ���� ��� �������","picture/67.htm","",0,25,300);
	      Menu1_2_5_9=new Array("� ������ ��� ������","picture/66.htm","",0,25,300);
	      Menu1_2_5_10=new Array("� ��������� ��� �������","picture/empaigmos.htm","",0,25,300);
          Menu1_2_5_11=new Array("� ������ ���� �� ��������","picture/65.htm","",0,25,300);
          Menu1_2_5_12=new Array("� ��������","picture/staurosi.htm","",0,25,300);
		  Menu1_2_5_13=new Array("� ����","picture/tafi.htm","",0,25,300);
		  Menu1_2_5_14=new Array("� ��������","picture/anastasi.htm","",0,25,300);
		  Menu1_2_5_15=new Array("� ����� �����","picture/ode.htm","",0,25,300);
		  Menu1_2_5_16=new Array("� �������� ��� ����","picture/philaphisi.htm","",0,25,300);
		  Menu1_2_5_17=new Array("� �������","picture/analipsi.htm","",0,25,300);
 
          Menu1_3=new Array("������� ��� ���������","","",5,20,150);

Menu1_3_1=new Array("�������","","",6,25,250);
   	        Menu1_3_1_1=new Array("� ����������","picture/pentikosti.htm","",0,25,250);
   	        Menu1_3_1_2=new Array("� ��������� ������","picture/16.htm","",0,25,200);
   	        Menu1_3_1_3=new Array("� ��������� ������","picture/17.htm","",0,25,200);
   	        Menu1_3_1_4=new Array("� ������������� ��������","picture/27.htm","",0,25,200);
   	        Menu1_3_1_5=new Array("� ��������� ������� � ����������","picture/iakovos.htm","",0,25,200);
			Menu1_3_1_6=new Array("� ����� ��������� � �����������","picture/86.htm","",0,25,200);
Menu1_3_2=new Array("�������� ��� �������","","",12,25,300);
   	        Menu1_3_2_1=new Array("� ����� �������� � ��������","picture/agiosignatios.htm","",0,25,250);
   	        Menu1_3_2_2=new Array("� ����� ����������","picture/78.htm","",0,25,200);
   	        Menu1_3_2_3=new Array("� ����� ��������","picture/87.htm","",0,25,200);
   	        Menu1_3_2_4=new Array("� ����� ���������","picture/agiosdimitrios.htm","",0,25,200);
            Menu1_3_2_5=new Array("� ����� ��������","picture/agiosgeorgios.htm","",0,25,200);
   	        Menu1_3_2_6=new Array("�� ����� ��������","picture/agiostheodoros1.htm","",0,25,200);
   	        Menu1_3_2_7=new Array("� ���� �����","picture/24.htm","",0,25,200);
            Menu1_3_2_8=new Array("� ���� ����������","picture/ekaterini.htm","",0,25,200);
            Menu1_3_2_9=new Array("� ���� ������","picture/irini.htm","",0,25,200);
            Menu1_3_2_10=new Array("� ���� �������","picture/varvara.htm","",0,25,200);
            Menu1_3_2_11=new Array("�� ����� ���������","picture/agioskosmas.htm","",0,25,200);
            Menu1_3_2_12=new Array("�� ����� ������������ ��� �����","picture/50.htm","",0,25,200);
                   

       Menu1_3_3=new Array("�������� ��� ���� ��� ���������","","",13,25,200);
                     Menu1_3_3_1=new Array("� ����� ��������� ","picture/84.htm","",0,25,220);
                     Menu1_3_3_2=new Array("� ����� ��������","picture/51.htm","",0,25,230);
                     Menu1_3_3_3=new Array("� ����� ���������","picture/83.htm","",0,25,230);
                     Menu1_3_3_4=new Array("� ����� ��������� � �������� ","picture/89.htm","",0,25,230);
					 Menu1_3_3_5=new Array("� ����� ������� � �����������","picture/90.htm","",0,25,230);
					 Menu1_3_3_6=new Array("� ����� ��������� ������","picture/80.htm","",0,25,230);
					 Menu1_3_3_7=new Array("� ����� ������","picture/92.htm","",0,25,230);
					 Menu1_3_3_8=new Array("� ����� ������������ � �� �����","picture/45.htm","",0,25,230);
					 Menu1_3_3_9=new Array("� ���� �������","picture/makrina.htm","",0,25,230);
					 Menu1_3_3_10=new Array("� ���� ����� � ��������","picture/33.htm","",0,25,230);
					 Menu1_3_3_11=new Array("� ����� �������� � ���������","picture/68.htm","",0,25,230);
					 Menu1_3_3_12=new Array("� ����� ������ ������","picture/34.htm","",0,25,230);
					 Menu1_3_3_13=new Array("� ����� ������� � �������","picture/agiosioannis.htm","",0,25,230);
 	      
     Menu1_3_4=new Array("�������� ���� ������","","",4,25,400);
	       Menu1_3_4_1=new Array("� ����� ���������� ����� �����","picture/silvestros.htm","",0,25,220);
	       Menu1_3_4_2=new Array("� ����� ��������� ����������","picture/36.htm","",0,25,400);
	       Menu1_3_4_3=new Array("� ���� ����","picture/olga.htm","",0,25,400);
	       Menu1_3_4_4=new Array("�� ����� �������� ��� ��������","picture/methodioskirilos.htm","",0,25,400);
             
             Menu1_3_5=new Array("������� ������","","",6,25,300);
	       Menu1_3_5_1=new Array("� ����� ��������� ��������","picture/agiosantonios.htm","",0,25,300);
		   Menu1_3_5_2=new Array("� ����� ��������� ����������������","picture/agiosathanasios.htm","",0,25,300);
		
		   Menu1_3_5_3=new Array("� ����� ������ � �������","picture/kosmas.htm","",0,25,300);
		   Menu1_3_5_4=new Array("�� ����� ������ �������� ��� ������","picture/rafael_irene_nic.htm","",0,25,300);
		   Menu1_3_5_5=new Array("�� ����� ��������� ��� ���������  �� ������","picture/40.htm","",0,25,300);
		   Menu1_3_5_6=new Array("� ����� �������� � ������","picture/41.htm","",0,25,300);
		  

