/***********************************************************************************
*	(c) Gennadios 2003                                                    *
***********************************************************************************/

	var NoOffFirstLineMenus=1;			// Number of first level items
	var LowBgColor='DCCBCB';			// Background color when mouse is not over
	var LowSubBgColor='DCCBCB';		// Background color when mouse is not over on subs
	var HighBgColor='FFFFFF';			// Background color when mouse is over
	var HighSubBgColor='FFFFFF';			// Background color when mouse is over on subs
	var FontLowColor='993300';			// Font color when mouse is not over
	var FontSubLowColor='993300';		// Font color subs when mouse is not over
	var FontHighColor='993300';			// Font color when mouse is over
	var FontSubHighColor='993300';		// Font color subs when mouse is over
	var BorderColor='993300';			// Border color
	var BorderSubColor='993300';			// Border color for subs
	var BorderWidth=2;				// Border width
	var BorderBtwnElmnts=1;			// Border between elements 1 or 0
	var FontFamily="tahoma,comic sans ms,technical"	// Font family menu items
	var FontSize=10;				// Font size menu items
	var FontBold=1;				// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered='left';			// Item text position 'left', 'center' or 'right'
	var MenuCentered='left';			// Menu horizontal position 'left', 'center' or 'right'
	var MenuVerticalCentered='top';		// Menu vertical position 'top', 'middle','bottom' or static
	var ChildOverlap=.2;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=.2;			// vertical overlap child/ parent
	var StartTop=05;				// Menu offset x coordinate
	var StartLeft=05;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=0;				// Multiple frames x correction
	var LeftPaddng=3;				// Left padding
	var TopPaddng=5;				// Top padding
	var FirstLineHorizontal=0;			// SET TO 1 FOR HORIZONTAL MENU, 0 FOR VERTICAL
	var MenuFramesVertical=1;			// Frames in cols or rows 1 or 0
	var DissapearDelay=1000;			// delay before menu folds in
	var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
	var FirstLineFrame='navig';			// Frame where first level appears
	var SecLineFrame='space';			// Frame where sub levels appear
	var DocTargetFrame='space';			// Frame where target documents appear
	var TargetLoc='';				// span id for relative positioning
	var HideTop=0;				// Hide first level when loading new document 1 or 0
	var MenuWrap=1;				// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var WebMasterCheck=0;			// menu tree checking on or off 1 or 0
	var ShowArrow=1;				// Uses arrow gifs when 1
	var KeepHilite=1;				// Keep selected path highligthed
	var Arrws=['common/tri.gif',5,10,'common/tridown.gif',10,5,'common/trileft.gif',5,10];	// Arrow source, width and height

function BeforeStart(){return}
function AfterBuild(){return}
function BeforeFirstOpen(){return}
function AfterCloseAll(){return}


// Menu tree
//	MenuX=new Array(Text to show, Link, background image (optional), number of sub elements, height, width);
//	For rollover images set "Text to show" to:  "rollover:Image1.jpg:Image2.jpg"

Menu1=new Array("������ �������","","",6,30,200);
	Menu1_1=new Array("� ������ ������� ��� � ����� ���","","",8,25,300);
			Menu1_1_1=new Array("��������� (��������� ��� ������)","data/mouseio/a/epoxi_pd/soumerioi/1.htm","",0,25,350);
			Menu1_1_2=new Array("��������� (���������� 7 �������)","data/mouseio/a/epoxi_pd/xananaioi/1.htm","",0,25,350);
			Menu1_1_3=new Array("��������� (���������� 12 �������)","data/mouseio/a/epoxi_pd/agupt/1.htm","",0,25,350);
			Menu1_1_4=new Array("�������� (���������� 15 �������)","data/mouseio/a/epoxi_pd/assyrioi/1.htm","",0,25,350);
			Menu1_1_5=new Array("���������� (���������� 4 �������)","data/mouseio/a/epoxi_pd/babylonioi/1.htm","",0,25,350);
			Menu1_1_6=new Array("������ (���������� 6 �������)","data/mouseio/a/epoxi_pd/perses/1.htm","",0,25,350);
			Menu1_1_7=new Array("������� (���������� 5 �������)","data/mouseio/a/epoxi_pd/ellines/1.htm","",0,25,350);
			Menu1_1_8=new Array("������� ������� �������� (���������� 2 �������)","data/mouseio/a/epoxi_pd/keimena_kd/1.htm","",0,25,350);
			
	Menu1_2=new Array("� ����������","","",2,25,200);
		Menu1_2_1=new Array("��������� �����","","",5,25,200);
			Menu1_2_1_1=new Array("� ���������� ��� ������ (���������� 2 �������)","data/mouseio/a/dimiourgia/bizantini/1.htm","",0,25,350);
			Menu1_2_1_2=new Array("� ���������� ��� �������� (���������� 1 ������)","data/mouseio/a/dimiourgia/bizantini/5.htm","",0,25,350);
			Menu1_2_1_3=new Array("� ���������� (��������� 1 ������)","data/mouseio/a/dimiourgia/bizantini/7.htm","",0,25,350);
			Menu1_2_1_4=new Array("���� ��� ���� (��������� 1 ������)","data/mouseio/a/dimiourgia/bizantini/10.htm","",0,25,350);
			Menu1_2_1_5=new Array("� ��� ��� � ����������� (���������� 3 �������)","data/mouseio/a/dimiourgia/bizantini/12.htm","",0,25,350);
		Menu1_2_2=new Array("������ �����","","",2,25,200);
			Menu1_2_2_1=new Array("� ���������� (���������� 3 �������)","data/mouseio/a/dimiourgia/ditiki/1.htm","",0,25,250);
			Menu1_2_2_2=new Array("� ����� ","data/mouseio/a/dimiourgia/ditiki/5.htm","",0,25,250);
			
	Menu1_3=new Array("�� ����������","","",3,25,200);
		Menu1_3_1=new Array("��������� �����","","",3,25,350);
			Menu1_3_1_1=new Array("� ��������� ��� ������ (���������� 2 �������)","data/mouseio/a/patriarxes/bizantini/1.htm","",0,25,350);
			Menu1_3_1_2=new Array("� ����� ��� ������ (��������� 1 ������)","data/mouseio/a/patriarxes/bizantini/4.htm","",0,25,350);
			Menu1_3_1_3=new Array("� ������� ��� ����� (���������� 2 �������)","data/mouseio/a/patriarxes/bizantini/8.htm","",0,25,350);			
		Menu1_3_2=new Array("������ �����","","",4,25,350);
			Menu1_3_2_1=new Array("� ������ (��������� 1 ������)","data/mouseio/a/patriarxes/ditiki/1.htm","",0,25,350);
			Menu1_3_2_2=new Array("� ����� ��� ������ (��������� 1 ������)","data/mouseio/a/patriarxes/ditiki/4.htm","",0,25,350);
			Menu1_3_2_3=new Array("���� ��� ����� (���������� 2 �������)","data/mouseio/a/patriarxes/ditiki/7.htm","",0,25,350);
			Menu1_3_2_4=new Array("����� ��� ����� (���������� 2 �������)","data/mouseio/a/patriarxes/ditiki/10.htm","",0,25,350);
		Menu1_3_3=new Array("�������� ��� ����������� (���������� 7 �������)","data/mouseio/a/patriarxes/ekthemata/1.htm","",0,25,350);
		
	Menu1_4=new Array("� ������","","",3,25,200);
		Menu1_4_1=new Array("��������� �����","","",8,25,400);
			Menu1_4_1_1=new Array("�� ������ ��� �����","data/mouseio/a/exodos/bizantini/1.htm","",0,25,450);
			Menu1_4_1_2=new Array("� ��������� �����","data/mouseio/a/exodos/bizantini/2.htm","",0,25,450);
			Menu1_4_1_3=new Array("������","data/mouseio/a/exodos/bizantini/3.htm","",0,25,450);
			Menu1_4_1_4=new Array("������� ��� ������� ��������","data/mouseio/a/exodos/bizantini/4.htm","",0,25,450);
			Menu1_4_1_5=new Array("������ ���� �����","data/mouseio/a/exodos/bizantini/5.htm","",0,25,450);
			Menu1_4_1_6=new Array("� �������� ��� ����� (���������� 4 �������)","data/mouseio/a/exodos/bizantini/6.htm","",0,25,450);
			Menu1_4_1_7=new Array("� ����� ��� � ������� ��� �������� (���������� 2 �������)","data/mouseio/a/exodos/bizantini/12.htm","",0,25,450);
			Menu1_4_1_8=new Array("� ������ ��� ����","data/mouseio/a/exodos/bizantini/15.htm","",0,25,450);
		Menu1_4_2=new Array("������ �����","","",6,25,400);
			Menu1_4_2_1=new Array("� ������� ��� ����� (��������� 1 ������)","data/mouseio/a/exodos/ditiki/1.htm","",0,25,300);
			Menu1_4_2_2=new Array("� ��������� �����","data/mouseio/a/exodos/ditiki/3.htm","",0,25,300);
			Menu1_4_2_3=new Array("� ������� ��� ������� ��������","data/mouseio/a/exodos/ditiki/4.htm","",0,25,300);
			Menu1_4_2_4=new Array("� ���� ��� �����","data/mouseio/a/exodos/ditiki/5.htm","",0,25,300);
			Menu1_4_2_5=new Array("������ ���� ����� (���������� 2 �������)","data/mouseio/a/exodos/ditiki/6.htm","",0,25,300);
			Menu1_4_2_6=new Array("� ������� ��� �����","data/mouseio/a/exodos/ditiki/9.htm","",0,25,300);
		Menu1_4_3=new Array("�������� ��� ����������� (���������� 9 �������)","data/mouseio/a/exodos/ekthemata/1.htm","",0,25,400);
		
		
	Menu1_5=new Array("�� ��������","","",3,25,200);
		Menu1_5_1=new Array("��������� �����","","",5,25,400);
			Menu1_5_1_1=new Array("� ������ �������","data/mouseio/a/basileis/bizantini/1.htm","",0,25,400);
			Menu1_5_1_2=new Array("� ������","data/mouseio/a/basileis/bizantini/2.htm","",0,25,400);
			Menu1_5_1_3=new Array("�� ������� ������ ��� ����� (���������� 2 �������)","data/mouseio/a/basileis/bizantini/3.htm","",0,25,400);
			Menu1_5_1_4=new Array("� �������� ����� (���������� 3 �������)","data/mouseio/a/basileis/bizantini/5b.htm","",0,25,400);
			Menu1_5_1_5=new Array("� �������� �������","data/mouseio/a/basileis/bizantini/9.htm","",0,25,400);
		Menu1_5_2=new Array("������ �����","","",2,25,400);
			Menu1_5_2_1=new Array("� ����� (��������� 1 ������)","data/mouseio/a/basileis/ditiki/1.htm","",0,25,400);
			Menu1_5_2_2=new Array("� �������� ������� (���������� 3 �������)","data/mouseio/a/basileis/ditiki/3.htm","",0,25,400);
		Menu1_5_3=new Array("�������� ��� ����������� (���������� 11 �������)","data/mouseio/a/basileis/ekthemata/1.htm","",0,25,400);
		
		
		
 	Menu1_6=new Array("�� ��������","","",3,25,200);
		Menu1_6_1=new Array("��������� �����","","",14,25,350);
			Menu1_6_1_1=new Array("� �������� ����� (���������� 2 �������)","data/mouseio/a/profites/bizantini/1.htm","",0,25,450);
			Menu1_6_1_2=new Array("� �������� ��������","data/mouseio/a/profites/bizantini/4.htm","",0,25,450);
			Menu1_6_1_3=new Array("� �������� �������","data/mouseio/a/profites/bizantini/5.htm","",0,25,450);
			Menu1_6_1_4=new Array("� �������� ������ ","data/mouseio/a/profites/bizantini/6.htm","",0,25,450);			
			Menu1_6_1_5=new Array("� �������� �������� (��������� ��� ������)","data/mouseio/a/profites/bizantini/8.htm","",0,25,450);
			Menu1_6_1_6=new Array("� �������� �������� (���������� 2 �������)","data/mouseio/a/profites/bizantini/10.htm","",0,25,450);
			Menu1_6_1_7=new Array("� �������� ��������","data/mouseio/a/profites/bizantini/12.htm","",0,25,450);
			Menu1_6_1_8=new Array("� �������� ����� (��������� ��� ������)","data/mouseio/a/profites/bizantini/15.htm","",0,25,450);
			Menu1_6_1_9=new Array("� �������� ����","data/mouseio/a/profites/bizantini/17.htm","",0,25,450);
			Menu1_6_1_10=new Array("� �������� ������ ��� �� ����� ������ (���������� 2 �������)","data/mouseio/a/profites/bizantini/18.htm","",0,25,450);
			Menu1_6_1_11=new Array("� �������� ��������","data/mouseio/a/profites/bizantini/21.htm","",0,25,450);
			Menu1_6_1_12=new Array("� �������� �������� (��������� ��� ������)","data/mouseio/a/profites/bizantini/22.htm","",0,25,450);
			Menu1_6_1_13=new Array("� �������� ��������","data/mouseio/a/profites/bizantini/24.htm","",0,25,450);
			Menu1_6_1_14=new Array("� �������� �����","data/mouseio/a/profites/bizantini/25.htm","",0,25,450);
    	Menu1_6_2=new Array("������ �����","","",4,25,350);
			Menu1_6_2_1=new Array("� �������� ����� (��������� 1 ������)","data/mouseio/a/profites/ditiki/1.htm","",0,25,350);
			Menu1_6_2_2=new Array("� �������� ������ (���������� 2 �������)","data/mouseio/a/profites/ditiki/3.htm","",0,25,350);
			Menu1_6_2_3=new Array("� �������� ���� (��������� 1 ������)","data/mouseio/a/profites/ditiki/6.htm","",0,25,350);
			Menu1_6_2_4=new Array("� �������� �������� (���������� 3 �������)","data/mouseio/a/profites/ditiki/8.htm","",0,25,350);
		Menu1_6_3=new Array("�������� ��� ����������� (��������� 1 ������)","data/mouseio/a/profites/ekthemata/1.htm","",0,25,350);
	