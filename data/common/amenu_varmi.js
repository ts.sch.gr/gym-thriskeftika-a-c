/***********************************************************************************
*	(c) Gennadios 2003                                                    *
***********************************************************************************/

	var NoOffFirstLineMenus=1;			// Number of first level items
	var LowBgColor='DCCBCB';			// Background color when mouse is not over
	var LowSubBgColor='DCCBCB';		// Background color when mouse is not over on subs
	var HighBgColor='FFFFFF';			// Background color when mouse is over
	var HighSubBgColor='FFFFFF';			// Background color when mouse is over on subs
	var FontLowColor='993300';			// Font color when mouse is not over
	var FontSubLowColor='993300';		// Font color subs when mouse is not over
	var FontHighColor='993300';			// Font color when mouse is over
	var FontSubHighColor='993300';		// Font color subs when mouse is over
	var BorderColor='993300';			// Border color
	var BorderSubColor='993300';			// Border color for subs
	var BorderWidth=2;				// Border width
	var BorderBtwnElmnts=1;			// Border between elements 1 or 0
	var FontFamily="tahoma,comic sans ms,technical"	// Font family menu items
	var FontSize=10;				// Font size menu items
	var FontBold=1;				// Bold menu items 1 or 0
	var FontItalic=0;				// Italic menu items 1 or 0
	var MenuTextCentered='left';			// Item text position 'left', 'center' or 'right'
	var MenuCentered='left';			// Menu horizontal position 'left', 'center' or 'right'
	var MenuVerticalCentered='top';		// Menu vertical position 'top', 'middle','bottom' or static
	var ChildOverlap=.2;				// horizontal overlap child/ parent
	var ChildVerticalOverlap=.2;			// vertical overlap child/ parent
	var StartTop=05;				// Menu offset x coordinate
	var StartLeft=05;				// Menu offset y coordinate
	var VerCorrect=0;				// Multiple frames y correction
	var HorCorrect=0;				// Multiple frames x correction
	var LeftPaddng=3;				// Left padding
	var TopPaddng=5;				// Top padding
	var FirstLineHorizontal=0;			// SET TO 1 FOR HORIZONTAL MENU, 0 FOR VERTICAL
	var MenuFramesVertical=1;			// Frames in cols or rows 1 or 0
	var DissapearDelay=1000;			// delay before menu folds in
	var TakeOverBgColor=1;			// Menu frame takes over background color subitem frame
	var FirstLineFrame='navig';			// Frame where first level appears
	var SecLineFrame='space';			// Frame where sub levels appear
	var DocTargetFrame='space';			// Frame where target documents appear
	var TargetLoc='';				// span id for relative positioning
	var HideTop=0;				// Hide first level when loading new document 1 or 0
	var MenuWrap=1;				// enables/ disables menu wrap 1 or 0
	var RightToLeft=0;				// enables/ disables right to left unfold 1 or 0
	var UnfoldsOnClick=0;			// Level 1 unfolds onclick/ onmouseover
	var WebMasterCheck=0;			// menu tree checking on or off 1 or 0
	var ShowArrow=1;				// Uses arrow gifs when 1
	var KeepHilite=1;				// Keep selected path highligthed
	var Arrws=['common/tri.gif',5,10,'common/tridown.gif',10,5,'common/trileft.gif',5,10];	// Arrow source, width and height

function BeforeStart(){return}
function AfterBuild(){return}
function BeforeFirstOpen(){return}
function AfterCloseAll(){return}


// Menu tree
//	MenuX=new Array(Text to show, Link, background image (optional), number of sub elements, height, width);
//	For rollover images set "Text to show" to:  "rollover:Image1.jpg:Image2.jpg"

Menu1=new Array("������� ��� ���������","","",6,30,200);
	Menu1_1=new Array("�������","","",3,25,350);
		Menu1_1_1=new Array("��������� �����","","",3,25,350);
			Menu1_1_1_1=new Array("� ����� �������� (���������� 10 �������)","data/mouseio/c/aparxes/bizantini/1.htm","",0,25,400);
			Menu1_1_1_2=new Array("� ��������� ������ (���������� 3 �������)","data/mouseio/c/aparxes/bizantini/18.htm","",0,25,400);
			Menu1_1_1_3=new Array("� ��������� ������ (���������� 4 �������)","data/mouseio/c/aparxes/bizantini/12.htm","",0,25,400);
		Menu1_1_2=new Array("������ �����","","",5,25,350);
			Menu1_1_2_1=new Array("� ���������� (���������� 8 �������)","data/mouseio/c/aparxes/ditiki/1.htm","",0,25,450);
			Menu1_1_2_2=new Array("� ��������� ������ (���������� 11 �������)","data/mouseio/c/aparxes/ditiki/10.htm","",0,25,450);
			Menu1_1_2_3=new Array("� ������������� �������� (���������� 5 �������)","data/mouseio/c/aparxes/ditiki/22.htm","",0,25,450);
			Menu1_1_2_4=new Array("� ����� ������� � ���������� (���������� 2 �������)","data/mouseio/c/aparxes/ditiki/28.htm","",0,25,450);
			Menu1_1_2_5=new Array("� ��������� ������ (���������� 11 �������)","data/mouseio/c/aparxes/ditiki/31.htm","",0,25,450);
		Menu1_1_3=new Array("�������� ��� ����������� (���������� 31 �������)","data/mouseio/c/aparxes/ekthemata/1.htm","",0,25,400);
	
	Menu1_2=new Array("��������, ������� ��� ����������","","",3,25,480);
		Menu1_2_1=new Array("��������� �����","","",5,25,350);
			Menu1_2_1_1=new Array("�������� ���������� (���������� 15 �������)","data/mouseio/c/diogmoi/bizantini/1.htm","",0,25,500);
			Menu1_2_1_2=new Array("�� �������������� ��������� ��� �������� (���������� 5 �������)","data/mouseio/c/diogmoi/bizantini/19.htm","",0,25,500);
			Menu1_2_1_3=new Array("����� ��������","data/mouseio/c/diogmoi/bizantini/25.htm","",0,25,500);
			Menu1_2_1_4=new Array("� ����� ������������ (���������� 2 �������)","data/mouseio/c/diogmoi/bizantini/26.htm","",0,25,500);
			Menu1_2_1_5=new Array("� ����� ��������� � ��������","data/mouseio/c/diogmoi/bizantini/29.htm","",0,25,500);
		Menu1_2_2=new Array("������ �����","","",2,25,350);
			Menu1_2_2_1=new Array("�������� (���������� 39 �������)","data/mouseio/c/diogmoi/ditiki/1.htm","",0,25,400);
			Menu1_2_2_2=new Array("� ����� ������������ (���������� 2 �������)","data/mouseio/c/diogmoi/ditiki/54.htm","",0,25,400);
		Menu1_2_3=new Array("�������� ��� ����������� (���������� 14 �������)","data/mouseio/c/diogmoi/ekthemata/1.htm","",0,25,400);
	
	Menu1_3=new Array("�������� ��� ����","","",3,25,480);
		Menu1_3_1=new Array("��������� �����","","",6,25,350);
			Menu1_3_1_1=new Array("��������� ���������� ��� ������������� (���������� 2 �������)","data/mouseio/c/edraiosi/bizantini/1.htm","",0,25,450);
			Menu1_3_1_2=new Array("�' ����������� ������� (���������� 4 �������)","data/mouseio/c/edraiosi/bizantini/4.htm","",0,25,450);
			Menu1_3_1_3=new Array("������� ��� ��������� (���������� 19 �������)","data/mouseio/c/edraiosi/bizantini/9.htm","",0,25,450);
			Menu1_3_1_4=new Array("������� ��� ������� (���������� 12 �������)","data/mouseio/c/edraiosi/bizantini/31.htm","",0,25,450);
			Menu1_3_1_5=new Array("� �' ����������� ������� (���������� 3 �������)","data/mouseio/c/edraiosi/bizantini/47.htm","",0,25,450);
			Menu1_3_1_6=new Array("����������� ����� (���������� 14 �������)","data/mouseio/c/edraiosi/bizantini/51.htm","",0,25,450);
		Menu1_3_2=new Array("������ ����� (���������� 7 �������)","data/mouseio/c/edraiosi/ditiki/1.htm","",0,25,350);
		Menu1_3_3=new Array("�������� ��� ����������� (���������� 17 �������)","data/mouseio/c/edraiosi/ekthemata/1.htm","",0,25,350);
	
	Menu1_4=new Array("��������� �������� ���� ������","","",2,25,480);
		Menu1_4_1=new Array("��������� ����� (��������� 1 ������)","data/mouseio/c/europi/bizantini/1.htm","",0,25,350);
		Menu1_4_2=new Array("������ �����","","",8,25,350);
			Menu1_4_2_1=new Array("��������� ������","data/mouseio/c/europi/ditiki/1.htm","",0,25,450);
			Menu1_4_2_2=new Array("� �����������","data/mouseio/c/europi/ditiki/4.htm","",0,25,450);
			Menu1_4_2_3=new Array("� ����� � ��������","data/mouseio/c/europi/ditiki/6.htm","",0,25,450);
			Menu1_4_2_4=new Array("����������� ���� ������ (���������� 3 �������)","data/mouseio/c/europi/ditiki/29.htm","",0,25,450);
			Menu1_4_2_5=new Array("� ����� ��������� (���������� 2 �������)","data/mouseio/c/europi/ditiki/8.htm","",0,25,450);
			Menu1_4_2_6=new Array("� ����� ���������","data/mouseio/c/europi/ditiki/11.htm","",0,25,450);
			Menu1_4_2_7=new Array("������� ��� �������� ������� (���������� 9 �������)","data/mouseio/c/europi/ditiki/12.htm","",0,25,450);
			Menu1_4_2_8=new Array("������������ (���������� 2 �������)","data/mouseio/c/europi/ditiki/22.htm","",0,25,450);
		
	Menu1_5=new Array("������� ������","","",3,25,480);
		Menu1_5_1=new Array("��������� ����� (���������� 8 �������)","data/mouseio/c/neotera_xronia/bizantini/2.htm","",0,25,350);
		Menu1_5_2=new Array("������ ����� (���������� 10 �������)","data/mouseio/c/neotera_xronia/ditiki/1.htm","",0,25,350);
		Menu1_5_3=new Array("�������� ��� �����������","","",6,25,350);
			Menu1_5_3_1=new Array("����������� ��� ���������� (���������� 11 �������)","data/mouseio/c/neotera_xronia/ekthemata/1.htm","",0,25,400);
			Menu1_5_3_2=new Array("������������ (���������� 6 �������)","data/mouseio/c/neotera_xronia/ekthemata/13.htm","",0,25,400);
			Menu1_5_3_3=new Array("��������������� (���������� 6 �������)","data/mouseio/c/neotera_xronia/ekthemata/20.htm","",0,25,400);
			Menu1_5_3_4=new Array("�������� ��� ������ (���������� 3 �������)","data/mouseio/c/neotera_xronia/ekthemata/21.htm","",0,25,400);
			Menu1_5_3_5=new Array("��������� ���������� ��� �������� (���������� 2 �������)","data/mouseio/c/neotera_xronia/ekthemata/25.htm","",0,25,400);
			Menu1_5_3_6=new Array("������ �������������� ������ (���������� 32 �������)","data/mouseio/c/neotera_xronia/ekthemata/30.htm","",0,25,400);
	
	Menu1_6=new Array("� ������������ ������ ������","","",4,25,350);
		Menu1_6_1=new Array("���������� ��� ������������� (���������� 25 �������)","data/mouseio/c/xris_simera/ekthemata/1.htm","",0,25,400);
		Menu1_6_2=new Array("� �������� ����������� (���������� 3 �������)","data/mouseio/c/xris_simera/ekthemata/28.htm","",0,25,400);
		Menu1_6_3=new Array("� ������������������ ������ (���������� 13 �������)","data/mouseio/c/xris_simera/ekthemata/37.htm","",0,25,400);
		Menu1_6_4=new Array("�� ��������� ��������� ��������� (���������� 6 �������)","data/mouseio/c/xris_simera/ekthemata/52.htm","",0,25,400);
	